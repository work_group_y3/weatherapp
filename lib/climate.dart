import 'package:flutter/material.dart';

enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade500,
      ),
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade500,
      ),
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.threesixty),
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}

//PreferredSizeWidget
AppBar buildAppBarWidget() {
  Icon cusIcon = Icon(Icons.search);
  Widget cusSearchBar = Text("AppBar");

  return AppBar(
    backgroundColor: Colors.white,
    leading: Icon(
      Icons.menu,
      color: Colors.black,
    ),
    actions: <Widget>[
      IconButton(
        onPressed: () {},
        icon: cusIcon,
        color: Colors.black,
      )
    ],
    title: TextField(
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white, //<-- SEE HERE
      ),
      cursorColor: Colors.black,
      style: TextStyle(color: Colors.black),
    ),
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Center(
            child: Stack(
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  child: Image.network(
                    'https://p0.pikist.com/photos/671/650/cloud-blue-sky-clouds-nature-color-landscape-atmosphere-background.jpg',
                    height: 250,
                    width: double.infinity,
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(15.0),
                  alignment: Alignment.center,
                  child: Text(
                    'อ.เมืองชลบุรี',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 28.0),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(55.0),
                  alignment: Alignment.center,
                  child: Text(
                    '28°',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 48.0),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(105.0),
                  alignment: Alignment.center,
                  child: Text(
                    'เมฆเป็นบางส่วน',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(120.0),
                  alignment: Alignment.center,
                  child: Text(
                    'สูงสุด:29° ตำสุด:21°',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 30,
            child: Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    "มีเฆมเป็นบางส่วนตลอดทั้งวัน",
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                iconTheme: IconThemeData(
                    // color: Colors.lightBlue,
                    ),
              ),
              child: profileActionItems(),
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            height: 30,
            child: Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    "พยากรณ์อากาศ 10 วัน",
                    style: TextStyle(
                      fontSize: 14,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Day1ListTile(),
          Day2ListTile(),
          Day3ListTile(),
          Day4ListTile(),
          Day5ListTile(),
          Day6ListTile(),
          Day7ListTile(),
          // Divider(
          //   color: Colors.grey,
          // ),
          // emailListTile(),
          // addressListTile(),
        ],
      ),
    ],
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.lightBlue,
        ),
        onPressed: () {},
      ),
      Text("25°"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.deepOrangeAccent,
        ),
        onPressed: () {},
      ),
      Text("38°"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.wb_sunny,
          color: Colors.deepOrangeAccent,
        ),
        onPressed: () {},
      ),
      Text("37°"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.sunny_snowing,
          color: Colors.redAccent,
        ),
        onPressed: () {},
      ),
      Text("35°"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.nightlight,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("29°"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.nights_stay,
          color: Colors.amber,
        ),
        onPressed: () {},
      ),
      Text("28°"),
    ],
  );
}

// ListTile
Widget Day1ListTile() {
  return ListTile(
    leading: Icon(
      Icons.cloud,
      color: Colors.lightBlue,
    ),
    title: Text('วันนี้'),
    subtitle: Text('28°'),
    trailing: Text(''),
  );
}

Widget Day2ListTile() {
  return ListTile(
    leading: Icon(
      Icons.cloud,
      color: Colors.lightBlue,
    ),
    title: Text('พ.'),
    subtitle: Text('29°'),
    trailing: Text(''),
  );
}

Widget Day3ListTile() {
  return ListTile(
    leading: Icon(
      Icons.sunny,
      color: Colors.deepOrangeAccent,
    ),
    title: Text('พฤ.'),
    subtitle: Text('30°'),
    trailing: Text(''),
  );
}

Widget Day4ListTile() {
  return ListTile(
    leading: Icon(
      Icons.cloud,
      color: Colors.lightBlue,
    ),
    title: Text('ศ.'),
    subtitle: Text('26°'),
    trailing: Text(''),
  );
}

Widget Day5ListTile() {
  return ListTile(
    leading: Icon(
      Icons.sunny,
      color: Colors.deepOrangeAccent,
    ),
    title: Text('ส.'),
    subtitle: Text('31°'),
    trailing: Text(''),
  );
}

Widget Day6ListTile() {
  return ListTile(
    leading: Icon(
      Icons.cloudy_snowing,
      color: Colors.indigo,
    ),
    title: Text('อา.'),
    subtitle: Text('28°'),
    trailing: Text(''),
  );
}

Widget Day7ListTile() {
  return ListTile(
    leading: Icon(
      Icons.water_drop,
      color: Colors.blue,
    ),
    title: Text('จ.'),
    subtitle: Text('25°'),
    trailing: Text(''),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text('63160005@as.th.com'),
    subtitle: Text('work'),
    trailing: IconButton(
      icon: Text(''),
      // color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text('99 Raya Chburi, Thailand'),
    subtitle: Text('home'),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      // color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}
